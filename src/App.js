import React, { useEffect, useState } from 'react';

const styles = {
  display: 'flex',
  alignItems: 'center',
  flexDirection: 'column',
  justifyContent: 'center',
};

function App({ delay }) {
  const [value, setValue] = useState(0);
  const [isRunning, setIsRunning] = useState(false);

  useEffect(() => {
    if (isRunning) {
      const intervalId = setInterval(() => {
        setValue(v => v + 1);
      }, delay);
      return () => {
        clearInterval(intervalId);
        setValue(0);
      };
    }
  }, [isRunning, delay]);

  return (
    <article style={styles}>
      <h1>{value}</h1>
      <button onClick={() => {
        setIsRunning(v => !v);
      }}>
        {isRunning ? 'stop' : 'start'}
      </button>
    </article>
  );
}

App.defaultProps = {
  delay: 1000,
};

export default App;
